libpath-dispatcher-perl (1.08-2) unstable; urgency=medium

  * Source-only no-change re-upload.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Jul 2020 19:09:14 +0200

libpath-dispatcher-perl (1.08-1) unstable; urgency=medium

  * Update debian/upstream/metadata.
  * Import upstream version 1.08.
  * Drop pod-abstract.patch, fixed upstream.
  * Add new (build) dependency.

 -- gregor herrmann <gregoa@debian.org>  Sun, 12 Jul 2020 12:22:27 +0200

libpath-dispatcher-perl (1.07-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 1.06
  * Update upstream copyright, delete paragraph for M::I (dropped upstream)

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Christine Spang from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Andrew Ruthven ]
  * New upstream release.
      - Fixes the dependency on Any::Moose by switching to Moo (Closes: #845804)
  * Change compat level to v9.
  * Update build and install dependencies.
  * Update upstream copyright years.
  * Re-upload to Debian (Closes: #963946)

  [ gregor herrmann ]
  * Drop fix-version-cmp.patch which was relevant for Perl 5.10.1.
  * Drop "Recommends: libpath-dispatcher-declarative-perl".
    The package was removed from the archive.
  * Update debian/* stanza in debian/copyright.
  * Install new upstream CONTRIBUTING file.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Update Vcs-* fields.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * debian/watch: use uscan version 4.
  * Set upstream metadata fields: Bug-Database, Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata.
  * Update Upstream-Contact in debian/copyright.
  * New upstream version 1.07
  * Add patch to add abstracts (whatis entries) to 2 POD texts (manpages).

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Jun 2020 18:24:45 +0200

libpath-dispatcher-perl (1.05-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Fixes "FTBFS with perl 5.18: t/031-structured-match.t"
    (Closes: #710965)
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Jun 2013 18:42:27 +0200

libpath-dispatcher-perl (1.04-1) unstable; urgency=low

  * New upstream release.
  * Drop build dependency on libtest-exception-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 02 Sep 2011 00:53:56 +0200

libpath-dispatcher-perl (1.03-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Set Standards-Version to 3.9.2 (no changes).
  * Bump debhelper compatibility level to 8.
  * Add libtest-fatal-perl to Build-Depends-Indep.

 -- gregor herrmann <gregoa@debian.org>  Wed, 31 Aug 2011 20:17:54 +0200

libpath-dispatcher-perl (1.02-1) unstable; urgency=low

  * New upstream release
  * Refresh copyright information
  * Add myself to Uploaders and Copyright
  * Standards-Version 3.9.1 (no changes)
  * Update build/runtime dependencies
  * Rewrite long description

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 27 Dec 2010 21:54:56 -0500

libpath-dispatcher-perl (0.15-2) unstable; urgency=low

  * Recommend libpath-dispatcher-declarative-perl and point to the new package
    in debian/NEWS (closes: #576639).
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Apr 2010 02:45:33 +0200

libpath-dispatcher-perl (0.15-1) unstable; urgency=low

  [ Nathan Handler ]
  * New upstream release
  * debian/copyright:
    - Update to use latest version of DEP5
    - Add myself to debian/* copyright
    - Correct upstream copyright
  * debian/control:
    - Add myself to list of Uploaders
    - Bump Standards-Version to 3.8.4 (no changes)
  * Use dpkg-source format 3.0

  [ gregor herrmann ]
  * Remove build and runtime dependency on libsub-exporter-perl.

 -- Nathan Handler <nhandler@ubuntu.com>  Sun, 28 Mar 2010 21:28:25 -0500

libpath-dispatcher-perl (0.14-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.3.

 -- Christine Spang <christine@debian.org>  Thu, 31 Dec 2009 16:30:37 -0500

libpath-dispatcher-perl (0.13-1) unstable; urgency=low

  * Initial Release. (Closes: #541049)

 -- Christine Spang <christine@debian.org>  Thu, 13 Aug 2009 05:36:58 -0400
